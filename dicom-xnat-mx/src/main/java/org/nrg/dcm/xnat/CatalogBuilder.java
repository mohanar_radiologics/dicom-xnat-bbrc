/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.CatalogBuilder
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.dcm.xnat;

import com.google.common.base.Supplier;
import com.google.common.collect.*;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.Utils;
import org.nrg.attr.Utils.NotRootDir;
import org.nrg.dcm.AttrAdapter;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.SOPModel;
import org.nrg.session.SessionBuilder;
import org.nrg.ulog.MicroLog;
import org.nrg.xdat.bean.CatDcmcatalogBean;
import org.nrg.xdat.bean.CatDcmentryBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public class CatalogBuilder implements Callable<Map<XnatResourcecatalogBean, CatDcmcatalogBean>> {
    private static final String RESOURCE_LABEL             = "DICOM";
    private static final String RESOURCE_LABEL_SECONDARY   = "secondary";
    private static final String RESOURCE_FORMAT            = "DICOM";
    private static final String RESOURCE_CONTENT           = "RAW";
    private static final String RESOURCE_CONTENT_SECONDARY = "secondary";

    private static final Map<?, String> EMPTY_CONSTRAINTS = Collections.emptyMap();

    private final Logger logger = LoggerFactory.getLogger(CatalogBuilder.class);
    private final String             id;
    private final MicroLog           log;
    private final DicomMetadataStore store;
    private final File               root;
    private final boolean            shouldLoadFiles;
    private final Map<Object, String> constraints = Maps.newLinkedHashMap();

    private Integer nFrames = null;

    public CatalogBuilder(final String id, final MicroLog log,
                          final DicomMetadataStore store, final File root,
                          final Map<?, String> constraints,
                          final boolean shouldLoadFiles) {
        this.id = id;
        this.log = log;
        this.store = store;
        this.root = root;
        if (null != constraints) {
            this.constraints.putAll(constraints);
        }
        this.shouldLoadFiles = shouldLoadFiles;
    }

    public CatalogBuilder(final String id, final MicroLog log,
                          final DicomMetadataStore store, final File root,
                          final boolean shouldLoadFiles) {
        this(id, log, store, root, EMPTY_CONSTRAINTS, shouldLoadFiles);
    }

    public CatalogBuilder(final String id, final MicroLog log,
                          final DicomMetadataStore store, final File root,
                          final Map<?, String> constraints) {
        this(id, log, store, root, constraints, false);
    }

    private static ListMultimap<URI, ExtAttrValue> getSortedFileValues(final DicomMetadataStore store, final Map<?, String> constraints)
            throws IOException, SQLException {
        final AttrAdapter fileAttrs = new AttrAdapter(store, constraints);
        fileAttrs.add(ImageFileAttributes.get());
        final ListMultimap<URI, ExtAttrValue> unsorted = fileAttrs.getValuesForResources();
        if (unsorted.isEmpty()) {
            throw new FileNotFoundException("scan contains no image files");
        }
        final ListMultimap<URI, ExtAttrValue> sorted = Multimaps.newListMultimap(
                new TreeMap<URI, Collection<ExtAttrValue>>(new ImageURIComparator(unsorted)),
                new Supplier<List<ExtAttrValue>>() {
                    public List<ExtAttrValue> get() {
                        return Lists.newArrayList();
                    }
                });
        sorted.putAll(unsorted);
        return ArrayListMultimap.create(sorted);
    }

    private List<ExtAttrValue>
    getCatalogValues(final Map<?, String> constraints) throws IOException {
        final AttrAdapter catalogAttrs = new AttrAdapter(store, constraints);
        catalogAttrs.add(CatalogAttributes.get());
        final Map<ExtAttrDef<DicomAttributeIndex>, Throwable> catalogFailures = Maps.newHashMap();
        final List<ExtAttrValue> catalogValues = org.nrg.session.SessionBuilder.getValues(catalogAttrs, catalogFailures);
        for (final Map.Entry<ExtAttrDef<DicomAttributeIndex>, Throwable> me : catalogFailures.entrySet()) {
            DICOMSessionBuilder.report(id, me.getKey(), me.getValue(), log);
        }
        return catalogValues;
    }

    public int getFrameCount() {
        if (null == nFrames) {
            throw new IllegalStateException("frame count not ready");
        } else {
            return nFrames;
        }
    }

    public Map<XnatResourcecatalogBean, CatDcmcatalogBean> call() throws IOException, SQLException {
        if (shouldLoadFiles) {
            final Map<String, String> addCols = Collections.singletonMap(SOPModel.XNAT_SCAN_COLUMN, id);
            store.add(Collections.singleton(root.toURI()), addCols);
            constraints.putAll(addCols);
        }

        final ListMultimap<URI, ExtAttrValue> fileValues = getSortedFileValues(store, constraints);
        final List<ExtAttrValue> catalogValues = getCatalogValues(constraints);

        // Build the catalog beans and resource records.
        final Map<XnatResourcecatalogBean, CatDcmcatalogBean> catalogs = Maps.newLinkedHashMap();
        try {
            final CatDcmcatalogBean catalog = new CatDcmcatalogBean();
            // Add an entry for each file to the catalog, and count total number of frames (z dimension)
            final Collection<URI> secondaryFiles = Lists.newArrayList();
            nFrames = 0;
            synchronized (MUTEX) {
                for (final URI uri : fileValues.keySet()) {
                    final List<ExtAttrValue> values = fileValues.get(uri);
                    final ExtAttrValue sopAttr = values.remove(0);
                    assert ImageFileAttributes.SOPClassUID.equals(sopAttr.getName()) : "expected SOP Class UID, found " + sopAttr;
                    final String fileSOPClass = sopAttr.getText();
                    if (SOPModel.isPrimaryImagingSOP(fileSOPClass)) {
                        int frames = 1;        // one frame per file unless otherwise specified
                        final CatDcmentryBean entry = new CatDcmentryBean();
                        for (final ExtAttrValue val : org.nrg.session.SessionBuilder.setValues(entry, values,
                                                                                               ImageFileAttributes.SOPClassUID, "URI", "numFrames")) {
                            if (ImageFileAttributes.SOPClassUID.equals(val.getName())) {
                                // ignore; we're done with this attribute
                                logger.debug("Done processing {}", val.getName());
                            } else if ("URI".equals(val.getName())) {
                                try {
                                    entry.setUri(Utils.getRelativeURI(root, new File(uri)));
                                } catch (NotRootDir e) {
                                    logger.error("session directory not root for image file " + uri, e);
                                }
                            } else {
                                assert "numFrames".equals(val.getName()) : "unexpected file attribute " + val.getName();
                                try {
                                    frames = Integer.valueOf(val.getText());
                                } catch (NumberFormatException e) {
                                    logger.warn("invalid frame count value " + val.getText() + " in " + uri);
                                }
                            }
                        }
                        catalog.addEntries_entry(entry);
                        nFrames += frames;
                    } else {
                        secondaryFiles.add(uri);
                    }
                }
            }
            if (nFrames > 0) {
                // There are some primary imaging data in this scan, so fill out
                // the primary catalog and build a resource for it.
                for (final ExtAttrValue val : org.nrg.session.SessionBuilder.setValues(catalog, catalogValues, "dimensions")) {
                    assert "dimensions".equals(val.getName());
                    // "dimensions" is set in both the series and the catalog.
                    // It's attributes-only and contains only "x" and "y";
                    // "z" must be computed from individual DICOM objects.
                    catalog.setDimensions_x(val.getAttrs().get("x"));
                    catalog.setDimensions_y(val.getAttrs().get("y"));
                }
                catalog.setDimensions_z(nFrames);

                final XnatResourcecatalogBean resource = new XnatResourcecatalogBean();
                resource.setLabel(RESOURCE_LABEL);
                resource.setFormat(RESOURCE_FORMAT);
                resource.setContent(RESOURCE_CONTENT);

                catalogs.put(resource, catalog);
            }

            // We might have some secondary files (i.e., those with different SOP class)
            // in the series as well
            if (!secondaryFiles.isEmpty()) {
                final CatDcmcatalogBean secondary = new CatDcmcatalogBean();
                for (final URI uri : secondaryFiles) {
                    final File file = new File(uri);
                    final CatDcmentryBean entry = new CatDcmentryBean();
                    for (final ExtAttrValue val : SessionBuilder.setValues(entry, fileValues.get(uri), "URI", "instanceNumber", "numFrames")) {
                        if ("URI".equals(val.getName())) {
                            try {
                                entry.setUri(Utils.getRelativeURI(root, file));
                            } catch (NotRootDir e) {
                                logger.error("session directory not root for image file " + file, e);
                            }
                        }
                        // ignore instanceNumber, numFrames, as they don't really apply to secondary files
                    }
                    secondary.addEntries_entry(entry);
                }
                final XnatResourcecatalogBean resource = new XnatResourcecatalogBean();
                resource.setLabel(RESOURCE_LABEL_SECONDARY);
                resource.setFormat(RESOURCE_FORMAT);
                resource.setContent(RESOURCE_CONTENT_SECONDARY);
                catalogs.put(resource, secondary);
            }

        } catch (IOException e) {
            log.log("Unable to save file for scan " + id, e);
        }

        return catalogs;
    }

    private static final Object MUTEX = new Object();
}
